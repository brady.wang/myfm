/*
Navicat MySQL Data Transfer

Source Server         : 33.10
Source Server Version : 50560
Source Host           : 192.168.33.10:3306
Source Database       : tp

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-03-06 08:55:49
*/

CREATE DATABASE `fm` /*!40100 DEFAULT CHARACTER SET utf8mb4 */
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'wang');
