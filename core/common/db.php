<?php

namespace core\common;

use core\config;

class db extends \PDO
{
    public function __construct($dsn='', $username='', $passwd='', $options=[])
    {
        try{
            $dsn = config('dsn','config_db');
            $username = config("user","config_db");
            $passwd = config("password","config_db");
            parent::__construct($dsn, $username, $passwd, $options);
        } catch(\Exception $e){
            echo $e->getMessage();
        }


    }
}
