<?php

if (!function_exists("dump")) {
    function dump($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }
}

if (!function_exists("p")) {
    function p($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }
}

//获取配置目录下的配置文件
function get_config_files()
{
    $handler = opendir(FM_PATH.'/config');//当前目录中的文件夹下的文件夹
    $files = [];
    while( ($filename = readdir($handler)) !== false ) {
        if($filename != "." && $filename != ".."){
            $files[] = $filename;
        }
    }
    closedir($handler);
    return $files;
}

/*
 * $key 配置的键名
 * $file config_site config_base 配置名 不要后缀
 */
function config($key,$file='')
{
    $value = '';

    if(!empty($file)){
        $config = include FM_PATH."/config/".$file.".php";
        if(isset($config[$key])){
            $value =  $config[$key];
        }
    } else {
        $files = get_config_files();
        foreach($files as $file){
            $config = include FM_PATH."/config/".$file;
            if(isset($config[$key])){
                $value =  $config[$key];
                break;
            }
        }
    }

    return $value;
}

function download_img($file_url, $save_to)
{
    $content = file_get_contents($file_url);
    file_put_contents("/www/img/".$save_to, $content);
    var_dump("download ".$save_to." success");
}