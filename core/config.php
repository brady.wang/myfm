<?php
namespace core;

class config
{

    public static function get($name, $file="config_base")
    {

        $path = CONFIG_PATH."/".$file.".php";
        if(is_file($path)){
            $config = include $path;
            if(isset($config[$name])){
                return $config[$name];
            } else {
                throw new \Exception("找不到配置".$path.":".$name);
            }
        } else {
            throw new \Exception("找不到配置文件".$path);
        }
    }

    public static function get_all($file="config_base")
    {
        $path = CONFIG_PATH."/".$file.".php";
        if(is_file($path)){
            $config = include $path;
            return $config;
        } else {
            throw new \Exception("找不到配置文件".$path);
        }
    }


}