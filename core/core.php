<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019/3/5
 * Time: 22:07
 */

namespace core;


class core
{
	public static function run()
	{
		self::setReporting();
		//self::unregisterGlobals();
		self::removeMagicQuotes();

		$route = new \core\route();
		$controller = $route->controller;
		$action = $route->action;
		$_GET = $route->params;

		$controller_file = APP_PATH."/controller/".$controller.".php";
		$controller_class = "\\app\\controller\\".$controller;
		if(is_file($controller_file)){
			include $controller_file;
			$controller_obj = new $controller_class();
			$controller_obj->$action();
		} else {
			throw new Exception("找不到控制器".$controller_class);
		}

	}

	// 检测开发环境
	public static function setReporting()
	{
		if (APP_DEBUG === true) {
			error_reporting(E_ALL);
			ini_set('display_errors','On');
		} else {
			error_reporting(E_ALL);
			ini_set('display_errors','Off');
			ini_set('log_errors', 'On');
			ini_set('error_log', RUNTIME_PATH. 'logs/error.log');
		}
	}


	// 删除敏感字符
	function stripSlashesDeep($value)
	{
		$value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
		return $value;
	}
	// 检测敏感字符并删除
	public  static function removeMagicQuotes()
	{
		if ( get_magic_quotes_gpc()) {
			$_GET = stripSlashesDeep($_GET );
			$_POST = stripSlashesDeep($_POST );
			$_COOKIE = stripSlashesDeep($_COOKIE);
			$_SESSION = stripSlashesDeep($_SESSION);
		}
	}
	// 检测自定义全局变量（register globals）并移除
	public  static function unregisterGlobals()
	{

		ini_set('register_globals',"On");
		if (ini_get('register_globals')) {
			$array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
			foreach ($array as $value) {
				foreach ($GLOBALS[$value] as $key => $var) {
					if ($var === $GLOBALS[$key]) {
						unset($GLOBALS[$key]);
					}
				}
			}
		}
	}
}