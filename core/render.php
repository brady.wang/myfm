<?php
namespace core;

class render
{
    public $params = [];
    public function assign($key,$value)
    {
        $this->params[$key] = $value;
    }

    public function display($view)
    {
        $file = APP_PATH."/view/".$view.".php";
        if(is_file($file)){
            extract($this->params);
            require_once $file;

        } else {
            throw new \Exception("视图".$file."不存在");
        }


    }
}