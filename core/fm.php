<?php

define("APP_PATH",FM_PATH."/app");
define("CONFIG_PATH",FM_PATH."/config");
define("LOG_PATH",FM_PATH."/log");
define("VENDOR_PATH",FM_PATH."/vendor");




include CORE_PATH."/autoload.php";
include CORE_PATH."/common/function.php";
include VENDOR_PATH."/autoload.php";


spl_autoload_register('\core\autoload::load');


\core\core::run();
