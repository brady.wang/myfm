<?php
namespace core;

class medoo extends \Medoo\Medoo{
    public function __construct(){
        $conf = \core\config::get_all('config_medoo');
        parent::__construct($conf);
    }
}