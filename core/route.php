<?php
/**
 * 路由控制
 */
namespace core;

class route{
    public $ctrl;

    public $controller;
    public $action ;
    public $params=array();
    public function __construct(){

        /**
         * 1、隐藏index.php
         * 2、获取URL中的控制器和方法
         * 3、获取URL中的参数
         */
        if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/'){
            $path = $_SERVER['REQUEST_URI'];
            $path_arr = explode('/',trim($path,'/'));
        } else {
            $this->controller = config::get("default_controller",'config_route');
            $this->action = config::get("default_action",'config_route');
        }

        if(isset($path_arr[0])){
            //控制器
            $this->controller = $path_arr[0];

            if(isset($path_arr[1])){
                //方法
                $this->action = $path_arr[1];
            } else {
                $this->action = config::get("default_action",'config_route');
            }

            //参数进行处理
            $i = 2;
            while($i< count($path_arr)){
                if(isset($path_arr[$i+1])){
                    $this->params[$path_arr[$i]] = $path_arr[$i+1];
                }
                $i = $i + 2;
            }


        } else {
            $this->controller = config::get("default_controller",'config_route');
            $this->action = config::get("default_action",'config_route');
        }

        $_GET = $this->params;

    }
}