<?php
namespace core;

class autoload
{
    public static function load($class_name){
        $file = FM_PATH."/". str_replace("\\","/",$class_name).'.php';
        if(file_exists($file)){
            include  $file ;
            return true;
        }else{
            echo 'error: unable to load '.$file;
            return false;
        }
    }
}